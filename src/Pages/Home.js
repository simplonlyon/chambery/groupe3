import React, { useState } from "react";
import List from '../components/List'; 
import Modal from '../components/Modal'; 
import FetchApi from '../components/FetchApi';
export default function Home() {
  const [persons, setPersons] = useState([]);
  const [selected, setSelected] = useState(null);
  const api = FetchApi(setPersons);
  

return (
    <section>
      {api}
      <List persons={persons} selectedPerson = {setSelected}/>
      <Modal selected={selected} setselect={setSelected}/>
    </section>
  )
}