import React, { useState } from 'react';
import Header from './components/Header';
import './App.css';
import Home from './Pages/Home';
import AddPerson from './Pages/AddPerson';

function App() {
  const [page, setPage] = useState('Home');

  const displayPage = () => {
    if(page === 'Home') {
      return <Home />;
    }
    if(page === 'Add Person') {
      return <AddPerson />
    }

  }

  return (
    <div className="App">
      <Header setpage = {setPage}/>
        <div className="container-fluid">
          {displayPage()}
        </div>
    </div>
  );
}

export default App;
