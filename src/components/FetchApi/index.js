import React, {useEffect} from 'react';

function FetchApi(callback) {
  let person = null; 
  useEffect(() => {
    fetch('http://localhost:4000/person')
      .then(response => response.json())
      .then(data => {
        person = callback(data);
      })
  }, []);
  return person; 
}

export default FetchApi;