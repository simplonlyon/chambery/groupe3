import React from 'react'; 

const List = (props) => {

  return (
    <div className="list-group">
      <h1>Persons List</h1>
        {props.persons.map(person => 
            <button key={person.id} onClick={() => props.selectedPerson(person)}className="list-group-item"> {person.name} {person.firstname}</button>
          )}
    </div>
  )

}

export default List; 