import React from 'react'; 

const Modal = (props) => {

return  <React.Fragment>
  {props.selected && 
    <div className="modal" tabIndex="-1" role="dialog" style={{display:'block'}}>
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
              <h5 className="modal-title">{props.selected.name}</h5>
              <button type="button" className="close" onClick={() => props.setselect(null)} >
                  <span>&times;</span>
              </button>
          </div>
          <div className="modal-body">
              <p>Firstname : {props.selected.firstname}</p>
            {props.selected.age === -1 &&
              <p>Age : deceased </p>
            }
            {props.selected.age > -1&& 
              <p>Age : {props.selected.age}</p>
            }
          </div>
        </div>
      </div>
    </div>
    
  }
  </React.Fragment>

}
export default Modal; 